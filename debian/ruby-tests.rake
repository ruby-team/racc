require 'gem2deb/rake/testtask'

if not ENV["AUTOPKGTEST_TMP"]
  load "Rakefile"
  Rake::Task["srcs"].invoke
end

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test', "test/lib"]
  t.ruby_opts = ["-rhelper"]
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
end
